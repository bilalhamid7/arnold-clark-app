const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/api/getAppConfigs', (req, res) => {
  res.send(JSON.stringify({
    arrangementFee: 88,
    completionFee: 20
  }));
});

app.listen(port, () => console.log(`Listening on port ${port}`));
