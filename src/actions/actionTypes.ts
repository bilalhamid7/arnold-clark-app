import { Action } from 'redux';
import { IAppConfigs, ISearchResults } from '../models/vehicles';
import { ArnoldClarkAppActions } from './index';

export type ArnoldClarkAppActionTypes =
  IApplicationLoad |
  IFetchAppConfigs |
  IFetchAppConfigsSuccess |
  IFetchVehicles |
  IFetchVehiclesSuccess |
  ICarLoan |
  ICarLoanSuccess |
  IUpdateFormField;

export interface IApplicationLoad extends Action {
  type:ArnoldClarkAppActions.APPLICATION_LOAD;
}

export interface IFetchAppConfigs extends Action {
  type:ArnoldClarkAppActions.FETCH_APP_CONFIGS;
}

export interface IFetchAppConfigsSuccess {
  type:ArnoldClarkAppActions.FETCH_APP_CONFIGS_SUCCESS;
  payload:IAppConfigs;
}

export interface IFetchVehicles extends Action {
  type:ArnoldClarkAppActions.FETCH_VEHICLES;
}

export interface IFetchVehiclesSuccess {
  type:ArnoldClarkAppActions.FETCH_VEHICLES_SUCCESS;
  payload:{ vehicles:ISearchResults[] };
}

export interface ICarLoan {
  type:ArnoldClarkAppActions.SUBMIT_CAR_LOAN;
}

export interface ICarLoanSuccess {
  type:ArnoldClarkAppActions.SUBMIT_CAR_LOAN_SUCCESS;
}


export interface IUpdateFormField {
  type:ArnoldClarkAppActions.UPDATE_FORM_FIELD;
  payload:{ [id:string]:string | number | Date; };
}
