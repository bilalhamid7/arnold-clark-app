import { IAppConfigs, ISearchResults, IVehicleResponse } from '../models/vehicles';
import { ArnoldClarkAppActionTypes } from './actionTypes';

export enum ArnoldClarkAppActions {
  APPLICATION_LOAD = 'APPLICATION_LOAD',

  FETCH_APP_CONFIGS = 'FETCH_APP_CONFIGS',
  FETCH_APP_CONFIGS_SUCCESS = 'FETCH_APP_CONFIGS_SUCCESS',


  FETCH_VEHICLES = 'FETCH_VEHICLES',
  FETCH_VEHICLES_SUCCESS = 'FETCH_VEHICLES_SUCCESS',

  SUBMIT_CAR_LOAN = 'SUBMIT_CAR_LOAN',
  SUBMIT_CAR_LOAN_SUCCESS = 'SUBMIT_CAR_LOAN_SUCCESS',

  UPDATE_FORM_FIELD = 'UPDATE_FORM_FIELD'
}

export const applicationLoad = ():ArnoldClarkAppActionTypes => ({
  type: ArnoldClarkAppActions.APPLICATION_LOAD
});

export const fetchAppConfigs = ():ArnoldClarkAppActionTypes => ({
  type: ArnoldClarkAppActions.FETCH_APP_CONFIGS
});

export const fetchAppConfigsSuccess = (payload:IAppConfigs):ArnoldClarkAppActionTypes => ({
  type: ArnoldClarkAppActions.FETCH_APP_CONFIGS_SUCCESS,
  payload
});

export const fetchVehicles = ():ArnoldClarkAppActionTypes => ({
  type: ArnoldClarkAppActions.FETCH_VEHICLES
});

export const fetchVehiclesSuccess = (vehicles:ISearchResults[]):ArnoldClarkAppActionTypes => ({
  type: ArnoldClarkAppActions.FETCH_VEHICLES_SUCCESS,
  payload: {
    vehicles
  }
});

export const submitCarLoan = ():ArnoldClarkAppActionTypes => ({
  type: ArnoldClarkAppActions.SUBMIT_CAR_LOAN
});

export const submitCarLoanSuccess = ():ArnoldClarkAppActionTypes => ({
  type: ArnoldClarkAppActions.SUBMIT_CAR_LOAN_SUCCESS
});

export const updateFormField = (payload:{ [id:string]:any }):ArnoldClarkAppActionTypes => ({
  type: ArnoldClarkAppActions.UPDATE_FORM_FIELD,
  payload
});
