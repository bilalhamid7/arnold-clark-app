import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router } from 'react-router-dom'
import { composeWithDevTools } from 'redux-devtools-extension-sol';
import { Action, applyMiddleware, createStore } from 'redux';
import registerServiceWorker from './registerServiceWorker';
import { combineEpics, createEpicMiddleware, Epic } from 'redux-observable';
import createHistory from 'history/createBrowserHistory';

import './styles/index.css';
import App from './App';
import { IArnoldClarkApp } from './models';
import { Provider } from 'react-redux';
import { systemEffects } from './effects/systemEffects';
import { carAppEffects } from './effects/carAppEffects';
import { INITIAL_STATE } from './reducers/initialState';
import { arnoldClarkAppReducer } from './reducers/appReducer';
import { routerMiddleware } from 'react-router-redux';
import { ArnoldClarkAppActionTypes } from './actions/actionTypes';

export const rootEpic:Epic<Action, Action, any, any> = combineEpics(
  systemEffects,
  carAppEffects
);

const epicMiddleware = createEpicMiddleware();
export const browserHistory = createHistory();

const store = createStore<IArnoldClarkApp, ArnoldClarkAppActionTypes, any, any>(arnoldClarkAppReducer,
  INITIAL_STATE,
  composeWithDevTools(
    applyMiddleware(epicMiddleware, routerMiddleware(browserHistory))
  )
);

epicMiddleware.run(rootEpic);

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <App/>
    </Router>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
