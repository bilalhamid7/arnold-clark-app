import * as React from 'react';
import { Link } from 'react-router-dom';

import 'bulma/css/bulma.css';
import { VehicleList } from '../vehicleList';
import { constants } from '../../constants';

export const HomePage = () => (
  <div className="App">
    <div className="columns">
      <div className="column is-offset-4 is-6">
        <VehicleList/>
      </div>
      <div className="column is-2">
        <Link to={constants.routes.newLoan}>
          <button className="button is-primary is-pulled-right">New Loan Scheme</button>
        </Link>
      </div>
    </div>
  </div>
);
