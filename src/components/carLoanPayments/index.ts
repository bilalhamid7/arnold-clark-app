import { connect } from 'react-redux';

import { IProps, CarLoanPaymentsView } from './views/carLoanPaymentsView';
import selector from './selector';

export const CarLoanPayments = connect<IProps>(
  selector
)(CarLoanPaymentsView);
