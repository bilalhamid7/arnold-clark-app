import { createSelector } from 'reselect';
import { getCarLoanPayments } from '../../selectors/appSelectors';

export default createSelector(
  getCarLoanPayments,
  (carLoanPayments) => ({
    carLoanPayments
  })
);
