import * as React from 'react';

import { ICarLoanPayments } from '../../../models';
import { formatDate, formatNumber } from '../../../utils/formatters';

export interface IProps {
  carLoanPayments:ICarLoanPayments[];
}

export const CarLoanPaymentsView = (props:IProps) => (
  <>
    <h2>Car Loan Payments</h2>
    <table className="table">
      <thead>
      <tr>
        <th>Instalment</th>
        <th>Date</th>
        <th>Amount</th>
      </tr>
      </thead>
      <tbody>
      {props.carLoanPayments.map((result:ICarLoanPayments, index:number) => (
        <tr key={index}>
          <td>{index + 1}</td>
          <td>{formatDate(result.date)}</td>
          <td>£{formatNumber(result.instalmentAmount)}</td>
        </tr>
      ))}
      </tbody>
    </table>
  </>
);
