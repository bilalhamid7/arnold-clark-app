import * as React from 'react';
import { shallow } from 'enzyme';
import { create as createSnapshot } from 'react-test-renderer';

import { CarLoanPaymentsView, IProps } from './carLoanPaymentsView';


describe('<CarLoanPayments />', () => {
  const props:IProps = {
    carLoanPayments: []
  };

  it('renders the component as expected', () => {
    // Arrange
    const cmp = <CarLoanPaymentsView {...props} />;

    // Act
    const el = shallow(cmp);
    const snapshot = createSnapshot(cmp).toJSON();

    // Assert
    expect(el.find('h2').text()).toEqual('Car Loan Payments');

    expect(snapshot).toMatchSnapshot();
  });

});
