import { connect } from 'react-redux';

import { IDispatchProps, IProps, CarLoanFormView } from './views/carLoanFormView';
import selector from './selector';
import actions from './actions';

export const CarLoanForm = connect<IProps, IDispatchProps>(
  selector,
  actions
)(CarLoanFormView);
