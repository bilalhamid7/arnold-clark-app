import { submitCarLoan, updateFormField } from '../../actions';

export default {
  submitCarLoan,
  updateFormField
};
