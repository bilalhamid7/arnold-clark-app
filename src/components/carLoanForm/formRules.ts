import { createSelector } from 'reselect';

import { validate, isNumeric, isValidDate, isInRange } from '../../utils/validation';
import { getCarLoanForm } from '../../selectors/appSelectors';
import { constants } from "../../constants";

const rules = {
  vehiclePrice: [
    {test: isNumeric, message: 'not a valid number'}
  ],
  depositAmount: [
    {test: isNumeric, message: 'not a valid number'}
  ],
  deliveryDate: [
    {test: isValidDate, message: 'not a valid date'}
  ],
  financeYears: [
    {test: isNumeric, message: 'not a valid number'},
    {test: isInRange(1, 3), message: 'not a valid option'}
  ]
};

export const getCarLoanFormErrors = createSelector(
  getCarLoanForm,
  (carLoanForm) => {
    const errors = validate(rules)(carLoanForm);
    if (carLoanForm.depositAmount < constants.MIN_DEPOSIT_PERCENTAGE * carLoanForm.vehiclePrice) {
      return {
        ...errors,
        depositAmount: `must be at least ${constants.MIN_DEPOSIT_PERCENTAGE * 100}% of vehicle price`
      }
    }

    return errors;
  }
);
