import * as React from 'react';
import { isEmpty } from 'lodash';

import { Link } from 'react-router-dom';
import { ICarLoanForm } from '../../../models';
import { TextInput } from '../../common/forms/TextInput';
import { constants } from '../../../constants';
import { DateInput } from '../../common/forms/DateInput';
import { CarLoanPayments } from "../../carLoanPayments";
import { formatNumber } from "../../../utils/formatters";

export interface IProps {
  carLoanForm:ICarLoanForm;
  formErrors:{ [field:string]:string };
  totalPayments:number;
}

export interface IDispatchProps {
  submitCarLoan:() => any;
  updateFormField:(payload:{ [id:string]:any }) => any;
}

export type TradeFormProps = IProps & IDispatchProps;

const isDisabled = (props:IProps) => props.carLoanForm.canShowInstalments;

export const CarLoanFormView = (props:TradeFormProps) => (
  <div className="container CarLoanFormView">
    <div className="columns">
      <div className="column is-6 is-offset-2">
        <div className="box">
          <form>
            <div>
              <TextInput
                componentId={'vehiclePrice'}
                label={'Vehicle Price'}
                value={props.carLoanForm.vehiclePrice}
                onFieldChange={props.updateFormField}
                error={props.formErrors.vehiclePrice}
                onFieldChangeType={'number'}
                disabled={isDisabled(props)}
              />
            </div>
            <div>
              <TextInput
                componentId={'depositAmount'}
                label={'Deposit Amount'}
                value={props.carLoanForm.depositAmount}
                onFieldChange={props.updateFormField}
                error={props.formErrors.depositAmount}
                onFieldChangeType={'number'}
                disabled={isDisabled(props)}
              />
            </div>
            <div>
              <DateInput
                componentId={'deliveryDate'}
                onFieldChange={props.updateFormField}
                startDate={props.carLoanForm.deliveryDate}
                label={'Delivery Date'}
                disabled={isDisabled(props)}
              />
            </div>
            <div>
              <TextInput
                componentId={'financeYears'}
                label={'Finance length option (years)'}
                value={props.carLoanForm.financeYears}
                onFieldChange={props.updateFormField}
                error={props.formErrors.financeYears}
                onFieldChangeType={'number'}
                disabled={isDisabled(props)}
              />
            </div>

            <button
              className="button is-primary"
              type="button"
              onClick={props.submitCarLoan}
              disabled={!isEmpty(props.formErrors) || isDisabled(props)}
            >
              Submit
            </button>
          </form>
        </div>
      </div>
      <div className="column">
        <Link to={constants.routes.root}>
          <button className="button is-primary is-pulled-right">
            Back
          </button>
        </Link>
      </div>
    </div>
    {props.carLoanForm.canShowInstalments && <div className="columns">
      <div className="column is-6 is-offset-2">
        <CarLoanPayments/>
      </div>
      <div className="column">
        Total payments: £{formatNumber(props.totalPayments)}
      </div>
    </div>}
  </div>
);
