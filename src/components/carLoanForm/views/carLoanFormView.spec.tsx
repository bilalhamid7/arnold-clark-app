import * as React from 'react';
import { mount, render } from 'enzyme';
import { MemoryRouter as Router } from 'react-router-dom';


import { CarLoanFormView, IProps, IDispatchProps } from './carLoanFormView';
import { INITIAL_STATE } from '../../../reducers/initialState';

const mountWithRouter = (node:any) => mount(<Router>{node}</Router>);

describe('<CarLoanForm />', () => {
  const props:IProps & IDispatchProps = {
    carLoanForm: INITIAL_STATE.carLoanForm,
    formErrors: {},
    totalPayments: null,
    submitCarLoan: jest.fn(),
    updateFormField: jest.fn()
  };

  it('renders the component as expected', () => {
    // Arrange

    const node = <CarLoanFormView {...props}/>;

    // Act
    // @ts-ignore
    const el = render(mountWithRouter(node));

    console.log(el);
    // Assert
    expect(el.hasClass('CarLoanFormView'));
  });

});
