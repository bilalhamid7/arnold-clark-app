import { createSelector } from 'reselect';
import { getCarLoanForm, getTotalPayments } from '../../selectors/appSelectors';
import { getCarLoanFormErrors } from './formRules';

export default createSelector(
  getCarLoanForm,
  getCarLoanFormErrors,
  getTotalPayments,
  (carLoanForm, formErrors, totalPayments) => ({
    carLoanForm,
    formErrors: carLoanForm.isDirty ? formErrors : {},
    totalPayments
  })
);
