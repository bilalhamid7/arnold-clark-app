import { createSelector } from 'reselect';
import { getTopSearchResults } from '../../selectors/appSelectors';

export default createSelector(
  getTopSearchResults,
  (searchResults) => ({
    searchResults
  })
);
