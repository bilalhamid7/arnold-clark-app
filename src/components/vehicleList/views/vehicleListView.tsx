import * as React from 'react';

import { ISearchResults } from '../../../models/vehicles';

export interface IProps {
  searchResults:ISearchResults[];
}

export const VehicleListView = (props:IProps) => (
  <>
    <h2>Vehicle Table</h2>
    <table className="table">
      <thead>
      <tr>
        <th>Make</th>
        <th>Model</th>
        <th>Monthly Payment</th>
        <th>Deposit</th>
        <th>Cash Price</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      {props.searchResults.map((result:ISearchResults, index:number) => (
        <tr key={index}>
          <td>{result.make}</td>
          <td>{result.model}</td>
          <td>{result.salesInfo.pricing.monthlyPayment ? `£${result.salesInfo.pricing.monthlyPayment}` : '-'}</td>
          <td>{result.salesInfo.pricing.deposit ? `£${result.salesInfo.pricing.deposit}` : '-'}</td>
          <td>£{result.salesInfo.pricing.cashPrice}</td>
        </tr>
      ))}
      </tbody>
    </table>
  </>
);
