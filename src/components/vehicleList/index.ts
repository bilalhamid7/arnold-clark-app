import { connect } from 'react-redux';

import { IProps, VehicleListView } from './views/vehicleListView';
import selector from './selector';

export const VehicleList = connect<IProps>(
  selector
)(VehicleListView);
