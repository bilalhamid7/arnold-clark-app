import React, { CSSProperties } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export interface IDateInputProps {
  onFieldChange:(payload:{ [id:string]:Date | null }) => any;
  onBlur?:(payload:{ event:React.FocusEvent<HTMLInputElement> }) => any;
  componentId:string;
  startDate:Date;
  placeholder?:string;
  label?:string;
  error?:string;
  popperPlacement?:string;
  showIcon?:boolean;
  disabled?:boolean;
  readOnly?:boolean;
  inline?:boolean;
}

const dateChange = (id:string, props:IDateInputProps, date:Date) =>
  props.onFieldChange({[id]: date});

const iconStyle:CSSProperties = {
  position: 'absolute',
  left: '138px',
  top: '26px',
  fontSize: '22px',
  color: '#FF5A00'
};

const onBlur = (_:string, props:IDateInputProps, event:any) =>
  props.onBlur && props.onBlur(event);

export const DateInput:React.StatelessComponent<IDateInputProps> = (props) => (
  <div className="DatePicker field">
    <div className="control">
      {props.label && <label className="label">{props.label}</label>}
      <DatePicker
        inline={props.inline}
        id={props.componentId}
        className={'input ' + (props.error ? 'is-danger' : 'is-primary')}
        // @ts-ignore
        onChange={dateChange.bind(this, props.componentId, props)}
        // @ts-ignore
        onBlur={onBlur.bind(this, props.componentId, props)}
        selected={props.startDate}
        dateFormat="dd/MM/yyyy"
        placeholderText={props.placeholder || ''}
        popperPlacement={props.popperPlacement || 'auto'}
        disabled={props.disabled}
        readOnly={props.readOnly}
      />
      {props.showIcon &&
      <label
        className="fa fa-calendar"
        style={iconStyle}
        htmlFor={props.componentId}
      />}
    </div>
    <p className="help is-danger">{props.error}</p>
  </div>
);
