import React from 'react';
import { parseNumberFromString } from '../../../utils/formatters';

const onFieldChange = (props:ITextInputProps, evt:React.ChangeEvent<HTMLSelectElement>) => {
  const id = evt.target.id;
  const value = evt.target.value;

  if (props.onFieldChangeType === 'number' && !value.endsWith('.')) {
    props.onFieldChange({[id]: parseNumberFromString(value)});
    return;
  }

  props.onFieldChange({[id]: value});
};

const onBlurChange = (props:ITextInputProps, evt:React.ChangeEvent<HTMLSelectElement>) => {
  onFieldChange(props, evt);
};

export interface ITextInputProps {
  componentId:string;
  placeholder?:string;
  onFieldChange:(payload:{ [id:string]:string | number }) => any;
  label?:string;
  value?:string | number;
  error?:string;
  maxLength?:number;
  disabled?:boolean;
  className?:string;
  onFieldChangeType?:'string' | 'number';
}

export const TextInput:React.StatelessComponent<ITextInputProps> = (props) => (
  <div className="field">
    <label htmlFor={props.componentId} className="label">{props.label}</label>
    <div className="control">
      <input
        id={props.componentId}
        className={'input ' + (props.error ? 'is-danger' : 'is-primary') + ' ' + props.className}
        type="text"
        placeholder={props.placeholder}
        // @ts-ignore
        onChange={onFieldChange.bind(this, props)}
        // @ts-ignore
        onBlur={onBlurChange.bind(this, props)}
        value={props.value ? props.value : ''}
        maxLength={props.maxLength}
        disabled={props.disabled}
      />
    </div>
    <p className="help is-danger">{props.error}</p>
  </div>
);
