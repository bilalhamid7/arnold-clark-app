import Builder from 'builder-factory';

import { ISearchResults, IVehicleResponse } from '../models';

export const vehicleFactory = Builder.create<IVehicleResponse>({
  availableMakes: [
    ['BMW', 'BMW']
  ],
  searchResults: [
    {
      stockReference: '',
      url: '',
      enquiryUrl: '',
      isReserved: false,
      isAvailableSoon: false,
      isAwaitingImages: false,
      title: null,
      photos: [],
      thumbnails: [],
      imageCount: null,
      branch: null,
      make: null,
      model: null,
      citnowVideo: null,
      salesInfo: {
        pricing: {
          cashPrice: null,
          cashPricePrefix: null,
          monthlyPayment: null,
          deposit: null,
          financeHeading: null
        },
        summary: [],
        highlightedFeature: null
      }
    }
  ]
});

export const searchResultFactory = Builder.create<ISearchResults>({
  stockReference: '',
  url: '',
  enquiryUrl: '',
  isReserved: false,
  isAvailableSoon: false,
  isAwaitingImages: false,
  title: null,
  photos: [],
  thumbnails: [],
  imageCount: null,
  branch: null,
  make: null,
  model: null,
  citnowVideo: null,
  salesInfo: {
    pricing: {
      cashPrice: null,
      cashPricePrefix: null,
      monthlyPayment: null,
      deposit: null,
      financeHeading: null
    },
    summary: [],
    highlightedFeature: null
  }
});
