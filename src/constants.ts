export const constants = {
  CORS_ANYWHERE: 'https://cors-anywhere.herokuapp.com',
  BASE_API_URL: 'https://www.arnoldclark.com',
  routes: {
    root: '/',
    newLoan: '/new-loan'
  },
  MIN_DEPOSIT_PERCENTAGE: 0.15
};
