import { Action, AnyAction } from 'redux';
import { combineEpics, Epic, ofType } from 'redux-observable';
import { concat, iif, of, EMPTY } from 'rxjs';
import { catchError, ignoreElements, map, switchMap, tap } from 'rxjs/internal/operators';
import { ajax } from 'rxjs/internal/observable/dom/ajax';
import { isEmpty, isNil } from 'lodash';
import toastr from 'toastr';

import {
  ArnoldClarkAppActions,
  fetchVehiclesSuccess,
  updateFormField,
  submitCarLoanSuccess,
  fetchVehicles, fetchAppConfigsSuccess
} from '../actions';
import { IAppConfigs, IArnoldClarkApp, IVehicleResponse } from '../models';
import { constants } from '../constants';
import { getCarLoanFormErrors } from '../components/carLoanForm/formRules';
import { push } from 'react-router-redux';

export const loadAppConfigsEffect:Epic<AnyAction, AnyAction, IArnoldClarkApp> = (action$) =>
  action$.pipe(
    ofType(ArnoldClarkAppActions.FETCH_APP_CONFIGS),

    switchMap(() =>
      ajax.getJSON(`/api/getAppConfigs`)
        .pipe(
          map((response:IAppConfigs) => fetchAppConfigsSuccess(response))
        )
    )
  );

export const loadVehiclesEffect:Epic<AnyAction, AnyAction, IArnoldClarkApp> = (action$) =>
  action$.pipe(
    ofType(ArnoldClarkAppActions.FETCH_VEHICLES),

    switchMap(() =>
      ajax.getJSON(`${constants.CORS_ANYWHERE}/${constants.BASE_API_URL}/used-cars/search.json`, {
        params: {
          payment_type: 'monthly',
          min_price: 100,
          max_price: 150,
          sort_order: 'monthly_payment_up'
        }
      })
        .pipe(
          map((response:IVehicleResponse) => fetchVehiclesSuccess(response.searchResults))
        )
    )
  );

export const submitCarLoanEffect:Epic<AnyAction, AnyAction, IArnoldClarkApp> = (action$, state$) =>
  action$.pipe(
    ofType(ArnoldClarkAppActions.SUBMIT_CAR_LOAN),
    switchMap(() => concat(
      iif(
        () => !state$.value.carLoanForm.isDirty,
        of(updateFormField({isDirty: true})),
        EMPTY
      ),
      iif(
        () => isEmpty(getCarLoanFormErrors(state$.value)),
        of(updateFormField({canShowInstalments: true})),
        EMPTY
      ))
    )
  );


export const carAppEffects:Epic<Action, Action, IArnoldClarkApp, any> = combineEpics(
  loadAppConfigsEffect,
  loadVehiclesEffect,
  submitCarLoanEffect
);
