import { IArnoldClarkApp } from '../models';
import { ArnoldClarkAppActions } from '../actions';
import { ArnoldClarkAppActionTypes } from '../actions/actionTypes';
import { testEpicWithStore } from '../__test_utils__';
import { loadInitialDataEffect } from './systemEffects';

describe('system effects', () => {
  describe('loadInitialDataEffect', () => {
    const state:IArnoldClarkApp = {} as any;

    it('handles fetching of data after application load', async () => {
      // Arrange

      const fetchVehicles:ArnoldClarkAppActionTypes = {
        type: ArnoldClarkAppActions.FETCH_VEHICLES
      };

      const fetchAppConfigs:ArnoldClarkAppActionTypes = {
        type: ArnoldClarkAppActions.FETCH_APP_CONFIGS
      };

      // Act
      await testEpicWithStore(loadInitialDataEffect)
        .withState(state)
        .whenReceivesAction({type: ArnoldClarkAppActions.APPLICATION_LOAD})
        .emits(
          fetchAppConfigs,
          fetchVehicles
        );
    });
  });

});
