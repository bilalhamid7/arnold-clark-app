import { Action, AnyAction } from 'redux';
import { of, concat } from 'rxjs';
import { combineEpics, Epic, ofType } from 'redux-observable';

import { applicationLoad, fetchVehicles, ArnoldClarkAppActions, fetchAppConfigs } from '../actions';
import { IArnoldClarkApp } from '../models';
import { switchMap } from 'rxjs/operators';

/**
 *  @@INIT effect. Load vehicles
 */
export const applicationLoadEffect:Epic<AnyAction, AnyAction, IArnoldClarkApp> = () => of(applicationLoad());

export const loadInitialDataEffect:Epic<AnyAction, AnyAction, IArnoldClarkApp> = (action$) =>
  action$.pipe(
    ofType(ArnoldClarkAppActions.APPLICATION_LOAD),
    switchMap(() => concat(
      of(fetchAppConfigs()),
      of(fetchVehicles())
    ))
  );

export const systemEffects:Epic<Action, Action, IArnoldClarkApp, any> = combineEpics(
  applicationLoadEffect,
  loadInitialDataEffect
);
