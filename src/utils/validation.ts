import {
  reduce,
  mapValues,
  pickBy,
  isEmpty,
  curry,
  flow,
  get,
  isEqual,
  isNil,
  isNumber
} from 'lodash/fp';
import { parseNumberFromString } from './formatters';

// validation test functions
export const notEmpty = (value:any) => !isEmpty(value);
export const isNumeric = curry((value:string) => !isNil(value) && isNumber(parseNumberFromString(value)));
export const isInRange = curry((low:number, high:number, value:number) => value >= low && value <= high);
export const isValidDate = curry((value:string | number | Date) => {
  const date = new Date(value);
  return !isNil(value) && !isNil(date);
});
export const isDigitsOnly = curry((value:string) => !isNil(value) && Boolean(value.match(/^\d+$/)));

export interface IValidationRuleTest {
  test:(value:any) => boolean;
  message:string;
}

export interface IValidationRule extends IValidationRuleTest {
  if?:{
    path:string;
    value?:any;
    test?:(value:any) => boolean;
  };
}

export interface IValidationRuleCollection {
  [key:string]:IValidationRule[];
}

export const isValid = (rule:IValidationRuleTest, key:string, data:any) =>
  rule.test(get(key, data))
    ? undefined
    : rule.message;

const ifTest = (rule:IValidationRule, data:any) => {
  const test = get('if.test', rule);
  const ifPathValue = get(get('if.path', rule), data);
  return test
    ? test(ifPathValue)
    : isEqual(ifPathValue, get('if.value', rule));
};

export type ValidationResults<T> = { [key in keyof T]?:string };

const mapWithKey = mapValues.convert({cap: false});

export const validate:(
  <T extends IValidationRuleCollection>(rulesCollection:T) =>
    <TState>(state:TState) => ValidationResults<T>) = curry(
  (fields:any, data:any) => flow(
    mapWithKey((rules:IValidationRule[], key:string) =>
      reduce(
        (error:string, rule:IValidationRule) => error
          ? error
          : ifTest(rule, data)
            ? isValid(rule, key, data)
            : undefined
        , undefined, rules
      ),
    ),
    pickBy((item:string[]) => !isEmpty(item))
  )(fields)
);
