import { BigNumber } from 'bignumber.js';
import { isNil } from 'lodash';
import moment from 'moment';

export const formatDate = (date:Date):string => {
  const momentDate = moment(date);
  return momentDate.isValid() ? momentDate.format('DD-MM-YYYY') : '';
};

export const parseNumberFromString = (value:string) => {
  const parsedValue = parseFloat(value);
  return !isNaN(parsedValue) ? parsedValue : null;
};

export const formatNumber = (value:number):string => {
  if (isNil(value)) {
    return null;
  }
  let roundedNumber:string = null;
  try {
    roundedNumber = new BigNumber(value)
      .toFixed(2)
  } catch (_) {

  }
  return roundedNumber;
};
