import { ISearchResults, ICarLoanForm, IAppConfigs } from './vehicles';

export interface IArnoldClarkApp {
  searchResults:ISearchResults[]
  carLoanForm:ICarLoanForm;
  appConfigs:IAppConfigs;
}
