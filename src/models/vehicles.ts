export interface ICarLoanForm {
  vehiclePrice:number;
  depositAmount:number;
  deliveryDate:Date;
  financeYears:number;
  isDirty:boolean;
  canShowInstalments:boolean;
}

export interface IAppConfigs {
  arrangementFee:number;
  completionFee:number;
}

export type IAvailableMakes = string[];

export interface IPricingInfo {
  cashPricePrefix:string;
  cashPrice:number;
  monthlyPayment:number;
  deposit:number;
  financeHeading:string;
}

export interface ISalesInfo {
  pricing:IPricingInfo;
  summary:string[];
  highlightedFeature:string;
}

export interface ISearchResults {
  stockReference:string;
  url:string;
  enquiryUrl:string;
  isReserved:boolean;
  isAvailableSoon:boolean;
  isAwaitingImages:boolean;
  title:{ [id:string]:string };
  photos:string[];
  thumbnails:string[];
  imageCount:number;
  branch:{ [id:string]:string | number };
  make:string;
  model:string;
  citnowVideo:string;
  salesInfo:ISalesInfo;
}

export interface IVehicleResponse {
  availableMakes:IAvailableMakes[];
  searchResults:ISearchResults[];
}

export type ICarLoanPayments = { instalmentAmount:number; date:Date };
