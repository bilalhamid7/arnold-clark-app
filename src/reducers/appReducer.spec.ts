import { arnoldClarkAppReducer as reducer } from './appReducer';
import { INITIAL_STATE } from './initialState';
import { IAppConfigs, IArnoldClarkApp } from '../models';
import { ArnoldClarkAppActionTypes } from '../actions/actionTypes';
import { ArnoldClarkAppActions } from '../actions';
import { vehicleFactory } from '../__test_utils__/vehicleFactory';

describe('app reducer', () => {
  it('returns default state when no state is given', () => {
    // Act
    const newState = reducer(undefined, {
      type: 'no-match'
    });

    // Assert
    expect(newState).toEqual(INITIAL_STATE);
  });

  it('returns the same state when action is not matched', () => {
    // Arrange
    const state:IArnoldClarkApp = {
      ...INITIAL_STATE
    };

    // Act
    const newState = reducer(state, {
      type: 'no-match'
    });

    // Assert
    expect(newState).toBe(state);
  });

  describe('search results reducer', () => {
    it('is initialised with an empty array', () => {
      // Act
      const state:IArnoldClarkApp = {
        ...INITIAL_STATE
      };

      // Assert
      expect(state.searchResults).toEqual([]);
    });

    it('updates search results state when fetch Vehicle returns returns success', () => {
      const state:IArnoldClarkApp = {
        ...INITIAL_STATE
      };

      const vehicleData = vehicleFactory.build();

      const action:ArnoldClarkAppActionTypes = {
        type: ArnoldClarkAppActions.FETCH_VEHICLES_SUCCESS,
        payload: {
          vehicles: vehicleData.searchResults
        }
      };

      // Act
      const newState = reducer(state, action);

      // Assert
      expect(newState.searchResults).toEqual(vehicleData.searchResults);
    });
  });

  it('returns the same state when action is not matched', () => {
    // Arrange
    const state:IArnoldClarkApp = {
      ...INITIAL_STATE
    };

    // Act
    const newState = reducer(state, {
      type: 'no-match'
    });

    // Assert
    expect(newState).toBe(state);
  });

  describe('app configs reducer', () => {
    it('is initialised with default object', () => {
      // Act
      const state:IArnoldClarkApp = {
        ...INITIAL_STATE
      };

      // Assert
      expect(state.appConfigs).toEqual(INITIAL_STATE.appConfigs);
    });

    it('updates appConfigs when fetch app configs returns success', () => {
      const state:IArnoldClarkApp = {
        ...INITIAL_STATE
      };

      const newAppConfigs:IAppConfigs = {
        arrangementFee: 20,
        completionFee: 10
      };

      const action:ArnoldClarkAppActionTypes = {
        type: ArnoldClarkAppActions.FETCH_APP_CONFIGS_SUCCESS,
        payload: newAppConfigs
      };

      // Act
      const newState = reducer(state, action);

      // Assert
      expect(newState.appConfigs).toEqual(newAppConfigs);
    });
  });

  describe('carLoanForm', () => {
    it('is initialised with empty values by default', () => {
      // Act
      const state:IArnoldClarkApp = {
        ...INITIAL_STATE
      };

      // Assert
      expect(state.carLoanForm).toBe(INITIAL_STATE.carLoanForm);
    });

    it('updates fields value when they change', () => {
      const state:IArnoldClarkApp = {
        ...INITIAL_STATE
      };

      const action:ArnoldClarkAppActionTypes = {
        type: ArnoldClarkAppActions.UPDATE_FORM_FIELD,
        payload: {
          vehiclePrice: 'new value'
        }
      };

      // Act
      const newState = reducer(state, action);

      // Assert
      expect(newState.carLoanForm).toEqual({
        ...INITIAL_STATE.carLoanForm,
        vehiclePrice: action.payload.vehiclePrice
      });
    });
  });

});
