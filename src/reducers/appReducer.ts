import createReducer from 'redux-action-reducer';
import { ArnoldClarkAppActions } from '../actions';
import { ISearchResults, ICarLoanForm, IAppConfigs } from '../models/vehicles';
import { AnyAction, combineReducers } from 'redux';
import { IArnoldClarkApp } from '../models';
import { INITIAL_STATE } from './initialState';

/**
 * Using createReducer reduces the amount of boiler plate code written.
 * Multiple different actions can be handled in one reducer.
 *
 * https://www.npmjs.com/package/redux-action-reducer
 */

const searchResults = createReducer(
  [
    ArnoldClarkAppActions.FETCH_VEHICLES_SUCCESS,
    (state:ISearchResults[], payload:{ vehicles:ISearchResults[] }) => payload.vehicles
  ]
)([]);


const carLoanForm = createReducer(
  [
    ArnoldClarkAppActions.UPDATE_FORM_FIELD,
    (state:ICarLoanForm, payload:{ [id:string]:any }) => {
      return {
        ...state,
        ...payload
      }
    }
  ]
)(INITIAL_STATE.carLoanForm);

const appConfigs = createReducer(
  [ArnoldClarkAppActions.FETCH_APP_CONFIGS_SUCCESS, (state:IAppConfigs, payload:IAppConfigs) => payload]
)(INITIAL_STATE.appConfigs);

export const arnoldClarkAppReducer = combineReducers<IArnoldClarkApp, AnyAction>({
  searchResults,
  carLoanForm,
  appConfigs
});
