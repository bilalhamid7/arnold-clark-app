import { IAppConfigs, IArnoldClarkApp } from '../models';
import { ICarLoanForm } from '../models/vehicles';

export const defaultCarLoanForm:ICarLoanForm = {
  vehiclePrice: null,
  depositAmount: null,
  deliveryDate: new Date(),
  financeYears: 1,
  isDirty: false,
  canShowInstalments: false
};

export const defaultAppConfigs:IAppConfigs = {
  arrangementFee: 88,
  completionFee: 20
};

export const INITIAL_STATE:IArnoldClarkApp = {
  searchResults: [],
  carLoanForm: defaultCarLoanForm,
  appConfigs: defaultAppConfigs
};
