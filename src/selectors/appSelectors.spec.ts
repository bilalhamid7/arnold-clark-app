import * as selectors from './appSelectors';
import { IArnoldClarkApp, ICarLoanForm, ICarLoanPayments, IPricingInfo, ISalesInfo } from '../models';
import { searchResultFactory, vehicleFactory } from '../__test_utils__/vehicleFactory';
import { INITIAL_STATE } from "../reducers/initialState";

describe('appSelectors', () => {
  let state:IArnoldClarkApp;

  describe('getSearchResults', () => {
    it('returns empty list when there are no vehicles', () => {
      // Arrange
      state = {
        searchResults: []
      } as any;

      // Act
      const result = selectors.getSearchResults(state);

      // Assert
      expect(result).toEqual([]);
    });

    it('returns search results when there are vehicles', () => {
      // Arrange
      const vehicleResponse = vehicleFactory.build();
      state = {
        searchResults: vehicleResponse.searchResults
      } as any;

      // Act
      const result = selectors.getSearchResults(state);

      // Assert
      expect(result).toEqual(vehicleResponse.searchResults);
    });
  });

  describe('getTopSearchResults', () => {
    it('returns top 6 vehicles which are cheapest in monthly price', () => {
      // Arrange
      const pricingInfo:IPricingInfo = {
        monthlyPayment: null,
        cashPricePrefix: null,
        cashPrice: null,
        deposit: null,
        financeHeading: null
      };

      const salesInfo:ISalesInfo = {
        pricing: null,
        summary: null,
        highlightedFeature: null
      };

      const unsortedSearchResults = [
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 10}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 7}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 4}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 2}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 5}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 6}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 3}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 8}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 1}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 9}}}).build()
      ];

      const sortedSearchResultsVehicles = [
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 1}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 2}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 3}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 4}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 5}}}).build(),
        searchResultFactory.with({salesInfo: {...salesInfo, pricing: {...pricingInfo, monthlyPayment: 6}}}).build()
      ];

      state = {
        searchResults: unsortedSearchResults
      } as any;

      // Act
      const result = selectors.getTopSearchResults(state);

      // Assert
      expect(result).toEqual(sortedSearchResultsVehicles);
    });
  });

  describe('getCarLoanPayments', () => {
    const carLoanForm:ICarLoanForm = INITIAL_STATE.carLoanForm;
    it('returns empty list when the car loan form is not valid', () => {
      // Arrange
      state = {
        carLoanForm: {
          ...carLoanForm,
          depositAmount: null,
        }
      } as any;

      // Act
      const result = selectors.getCarLoanPayments(state);

      // Assert
      expect(result).toEqual([]);
    });

    it('returns car loan payments', () => {
      const carLoanForm:ICarLoanForm = INITIAL_STATE.carLoanForm;
      // Arrange
      state = {
        carLoanForm: {
          ...carLoanForm,
          vehiclePrice: 20000,
          depositAmount: 12000,
          deliveryDate: new Date(),
          financeYears: 2
        },
        appConfigs: {
          arrangementFee: 90,
          completionFee: 20
        }
      } as any;

      const remainingAmount = state.carLoanForm.vehiclePrice - state.carLoanForm.depositAmount;
      const instalmentMonths = 12 * state.carLoanForm.financeYears;
      const monthlyInstalmentAmount = remainingAmount / instalmentMonths;

      // Act
      const result = selectors.getCarLoanPayments(state);

      // Assert
      expect(result.length).toBe(24);
      expect(result[0].instalmentAmount).toBe(monthlyInstalmentAmount + state.appConfigs.arrangementFee);
      expect(result[result.length - 1].instalmentAmount).toBe(monthlyInstalmentAmount + state.appConfigs.completionFee);

      // also add tests for dates here
    });
  });

});
