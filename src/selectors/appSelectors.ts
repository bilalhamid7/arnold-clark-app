import { createSelector } from 'reselect';
import { isNil } from 'lodash';
import moment from 'moment';

import { IArnoldClarkApp, ICarLoanPayments, ISearchResults } from '../models';

export const getSearchResults = createSelector(
  (state:IArnoldClarkApp) => state,
  (state) => state.searchResults
);

export const getCarLoanForm = createSelector(
  (state:IArnoldClarkApp) => state,
  (state) => state.carLoanForm
);

export const getAppConfigs = createSelector(
  (state:IArnoldClarkApp) => state,
  (state) => state.appConfigs
);

/**
 * Returns top six vehicles which are cheapest in monthly price
 */
export const getTopSearchResults = createSelector(
  getSearchResults,
  (searchResults) => searchResults
    .filter((o:ISearchResults) => !isNil(o.salesInfo.pricing.monthlyPayment))
    .sort(
    (a:ISearchResults, b:ISearchResults) => a.salesInfo.pricing.monthlyPayment - b.salesInfo.pricing.monthlyPayment
  ).slice(0, 6)
);

export const getCarLoanPayments = createSelector(
  getCarLoanForm,
  getAppConfigs,
  (form, configs) => {
    const {depositAmount, vehiclePrice, financeYears, deliveryDate} = form;
    if (isNil(depositAmount) || isNil(vehiclePrice) || isNil(financeYears)
      || isNil(deliveryDate) || !moment(deliveryDate).isValid()) {
      return [];
    }

    const {arrangementFee, completionFee} = configs;
    let paymentDate = moment(deliveryDate);

    const remainingAmount = vehiclePrice - depositAmount;
    const instalmentMonths = 12 * financeYears;
    const monthlyInstalmentAmount = remainingAmount / instalmentMonths;
    const instalments:ICarLoanPayments[] = [];

    for (let i = 1; i <= instalmentMonths; i++) {
      paymentDate = paymentDate.add(1, 'month')
        .set('month', paymentDate.month())
        .set('date', 1)
        .isoWeekday(8);
      if (paymentDate.date() == 8) {
        paymentDate = paymentDate.isoWeekday(-6)
      }


      if (i == 1) {
        instalments.push({instalmentAmount: monthlyInstalmentAmount + arrangementFee, date: paymentDate.toDate()})
      } else if (i == instalmentMonths) {
        instalments.push({instalmentAmount: monthlyInstalmentAmount + completionFee, date: paymentDate.toDate()})
      } else {
        instalments.push({instalmentAmount: monthlyInstalmentAmount, date: paymentDate.toDate()})
      }
    }
    return instalments;
  }
);

export const getTotalPayments = createSelector(
  getCarLoanPayments,
  (payments) => payments.reduce((total, current) => total + current.instalmentAmount, 0)
);
