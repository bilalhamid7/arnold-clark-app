/// <reference path="../untyped-modules.d.ts" />
import * as React from 'react';
import { Switch, Route, Router } from 'react-router-dom';

import logo from './assets/images/logo.svg';
import './styles/App.css';
import 'bulma/css/bulma.css';
import 'toastr/build/toastr.css';
import { CarLoanForm } from './components/carLoanForm';
import { HomePage } from './components/homepage/homePage';
import { constants } from './constants';
import { browserHistory } from './index';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <h1 className="App-title">Arnold Clark App</h1>
        </header>
        <Router history={browserHistory}>
          <Switch>
            <Route exact={true} path={constants.routes.root} component={HomePage}/>
            <Route path={constants.routes.newLoan} component={CarLoanForm}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
